# Navigation Design

Everything in this directory is for the creation of a universal navigation bar for all of Fedora's websites.

## Main Focus

- Fedora has many websites that are hosted in different places and the Fedora community has many teams who work at different timelines and in different spaces
- This navigation aims to make it easy for anyone to move around Fedora's web ecosystem without getting lost or to have to rely on external tools like bookmarks etc to get to any given web space
- This navigation design needs to be flexible. The ability to accomodate new pages or lose pages depending on community and team needs is an essential feature for the design
- The navigation needs to be accessible and easy to use
  - People who are/have:
    - blind
    - color blind
    - non-English/non-native English speakers
    - learning disabilities
  - It needs to be complex enough to accomodate the awesomeness that is the Fedora community while still being simple enough to navigate that people actually use it.
- The navigation needs to be exportable
  - It needs to be able to be loaded onto webpages not maintained by the Fedora Web and Apps team.
    - So we need a lightweight exportable version that will be able to be kept up to date and not require maintenance work on a repo to repo basis
    - It may also be useful to offer communities a lighter variant that has the primary navigation stuff needed for it's context, and feels consistent with the full nav, but doesn't have extra code etc
  - Easy to maintain
    - The codebase shouldn't feel like a nightmare to have to edit

### Why is this needed?

Fedora is a growing community and is also growing in use among non-technical people and people who aren't familiar with the ecosystem. An early barrier to participation is trying to navigate around and learn where to go - which has seen great work done on by members of the community, but our web infrastructure does little to address this barrier.

It will also improve contributor experience by making it easier to get around quickly

## Design Concept

### Background and Inspiration

The initial design concept is based on a review of the nav used in the latest mockups from the Fedora Design team in conjunction with analyses of the navigation systems used at Microsoft and Apple. It also includes some philosophical ideas present in the Gnome Desktop (particularly on getting out of the user's way and presenting only relevant information).

When reviewing these navigation systems, a **context based** design seemed to make the most sense regarding Fedora's broad number of web pages and focuses.

### Context Based Navigation

This system would have 4 primary contexts that websites would be grouped under.

- Editions
- Community
- Contributors
- Support

While Community and Contributors has a lot of overlap, the main difference is that **Community** is for: Communication spaces, events, publications.

Wheras **Contributors** is for active work on projects: Gitlab, Pagure, Accounts.

These may be merged together, however that's a topic for the second round of testing.

---
